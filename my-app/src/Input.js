import { Component } from "react";

export class Input extends Component {
  constructor(props) {
    super(props);
    this.state = {
      type: "password",
    };
  }

  showOrHidePassword = () => {
    this.state.type === "password"
      ? this.setState({ type: "text" })
      : this.setState({ type: "password" });
  };

  render() {
    return (
      <div>
        <input type={this.state.type}></input>
        <button onClick={this.showOrHidePassword}>See your password</button>
      </div>
    );
  }
}
