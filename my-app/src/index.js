import React from "react";
import ReactDOM from "react-dom/client";
import { Input } from "./Input";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(<Input />);
